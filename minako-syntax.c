#include <stdio.h>
#include <stdlib.h>
#include "minako.h"

int currentToken;
int nextToken;
int lineNo;

void s_program();
void s_functionDefinition();
void s_type();
void s_statementList();
void s_functioncall();
void s_block();
void s_statement();
void s_ifStatement();
void s_returnStatement();
void s_printf();
void s_statAssignment();
void s_simpExpr();
void s_expr();
void s_term();
void s_factor();
void s_assignment();

int isToken(int derivedToken) {
	if (currentToken == derivedToken)
		return 1;
	else
		return 0;
}

void eat() {
	if (nextToken == EOF)
		currentToken = nextToken;
	else
	{
		lineNo = yylineno;
		currentToken = nextToken;
		nextToken = yylex();
	}
}

void isTokenAndEat(int derivedToken) {
	if (isToken(derivedToken))
		eat();
	else
	{
		fprintf(stderr, "ERROR: Syntax error in line %d\n", lineNo);
		exit(1);
	}
}

void s_program() {
	while (!isToken(EOF))
	{
		s_functionDefinition();
	}
}

void s_functionDefinition() {
	s_type();
	isTokenAndEat(ID);
	isTokenAndEat('(');
	isTokenAndEat(')');
	isTokenAndEat('{');
	s_statementList();
	isTokenAndEat('}');
}

void s_type() {
	if (currentToken == KW_BOOLEAN || currentToken == KW_FLOAT || currentToken == KW_INT || currentToken == KW_VOID)
		eat();
	else
		isTokenAndEat(KW_BOOLEAN);
		//will trigger a announcement: invalid syntax 
}

void s_statementList() {
	while (!isToken('}'))
	{
		s_block();
	}
}

void s_block() {
	if (isToken('{'))
	{
		isTokenAndEat('{');
		s_statementList();
		isTokenAndEat('}');
	}
	else
	{
		s_statement();
	}
}

void s_returnStatement() {
	isTokenAndEat(KW_RETURN);
	if (!isToken(';'))
		s_assignment();
}

void s_ifStatement() {
	isTokenAndEat(KW_IF);
	isTokenAndEat('(');
	s_assignment();
	isTokenAndEat(')');
	s_block();
}

void s_printf() {
	isTokenAndEat(KW_PRINTF);
	isTokenAndEat('(');
	s_assignment();
	isTokenAndEat(')');
}

void s_statement() {
	switch (currentToken)
	{
	case (KW_IF):
		s_ifStatement();
		break;
	case (KW_RETURN):
		s_printf();
		isTokenAndEat(';');
		break;
	case (ID):
		if (nextToken == '=')
		{
			s_statAssignment();
			isTokenAndEat(';');
		}
		else if (nextToken == '(')
		{
			s_functioncall();
			isTokenAndEat(';');
		}
		else
		{
			isTokenAndEat('=');
		}
		break;
	default:
		isTokenAndEat(KW_IF);
		//will trigger a announcement: invalid syntax 
		break;
	}
}

void s_functioncall() {
	isTokenAndEat(ID);
	isTokenAndEat('(');
	isTokenAndEat(')');
}

void s_statAssignment() {
	isTokenAndEat(ID);
	isTokenAndEat('=');
	s_assignment();
}

void s_assignment() {
	if (isToken(ID) && (nextToken == '='))
	{
		isTokenAndEat(ID);
		isTokenAndEat('=');
		s_assignment();
	}
	else
		s_expr();
}

void s_expr() {
	s_simpExpr();
	if (!isToken(')'))
	{
		if (isToken(EQ) || isToken(NEQ) || isToken(LEQ) || isToken(GEQ) || isToken(LSS) || isToken(GRT))
		{
			eat();
			s_simpExpr();
		}
	}
}

void s_simpExpr() {
	if (isToken('-'))
		eat();
	s_term();
	while (isToken('+') || isToken('-') || isToken(OR))
	{
		eat();
		s_term();
	}
}

void s_term() {
	s_factor();
	while (isToken('*') || isToken('/') || isToken(AND))
	{
		eat();
		s_factor();
	}
}

void s_factor() {
	if (isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN))
		eat();
	else {
		if (isToken(ID)) {
			if (nextToken == '(') {
				s_functioncall();
			}
			else {
				isTokenAndEat(ID);
			}
		}
		else {
			isTokenAndEat('(');
			s_assignment();
			isTokenAndEat(')');
		}

	}
}

int main(int argc, char* argv[]){
	if (argc != 2)
		yyin = stdin;
	else
	{
		yyin = fopen(argv[1], "r");
		if (yyin == 0)
		{
			fprintf(stderr, "ERROR: Could not open file %s for reading.\n", argv[1]);
			exit(-1);
		}
	}
	
	//init Vlaues
	currentToken = yylex();
	nextToken = yylex();

	s_program();

	return 0;
}
